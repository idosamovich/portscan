package scanners


type PortState int

const (
	PortStateOpen = 0
	PortStateClosed = 1
	PortStateFiltered = 2
	PortStateUnknown = 3
)

var PortStateTranslation = map[PortState]string{
	PortStateOpen: "open",
	PortStateClosed: "closed",
	PortStateFiltered: "filtered",
	PortStateUnknown: "open/filtered",
}

type PortScanner interface {
	Scan(srcPort uint16, dstPort uint16) (ScanResult, error)
}

type ScanResult struct {
	Port uint16
	State PortState
}
