package scanners

import (
	"net"
	"time"
	"errors"

	"github.com/google/gopacket"
	"github.com/google/gopacket/layers"
	"github.com/google/gopacket/pcap"
	"github.com/google/gopacket/routing"
)

const (
	maxPacketRead = 65535
	defaultPacketsToSend = 3
	defaultFilteredTimeout = 3 * time.Second
)

type baseScanner struct {
	iface *net.Interface
	srcMAC net.HardwareAddr
	gatewayMAC net.HardwareAddr
	srcIP net.IP
	dstIP net.IP

	serializeOptions gopacket.SerializeOptions
	serializeBuffer  gopacket.SerializeBuffer

	// The number of packets we send for each scan, in case of packet loss.
	packetsToSend int
}


func newBaseScanner(scanIP net.IP) (*baseScanner, error) {
	router, err := routing.New()
	if err != nil {
		return nil, err
	}

	iface, gatewayIP, srcIP, err := router.Route(scanIP)
	if err != nil {
		return nil, err
	}

	if gatewayIP == nil {
		gatewayIP = scanIP
	}
	gatewayMAC, err := getGatewayMAC(gatewayIP, srcIP, iface.HardwareAddr, iface)
	if err != nil {
		return nil, err
	}

	return &baseScanner{
		iface: iface,
		srcMAC: iface.HardwareAddr,
		gatewayMAC: gatewayMAC,
		srcIP: srcIP,
		dstIP: scanIP,

		// Options for gopacket packet serialization.
		// We set them so that we won't have to do it manually.
		serializeOptions: gopacket.SerializeOptions{
			FixLengths:       true,
			ComputeChecksums: true,
		},
		serializeBuffer: gopacket.NewSerializeBuffer(),
		packetsToSend: defaultPacketsToSend,
	}, nil
}

func getGatewayMAC(gatewayIP net.IP, srcIP net.IP, srcMAC net.HardwareAddr, iface *net.Interface) (net.HardwareAddr, error) {
	handle, err := pcap.OpenLive(iface.Name, maxPacketRead, true, pcap.BlockForever)
	if err != nil {
		return nil, err
	}

	
	for i:=0; i<defaultPacketsToSend; i++ {
	if err := sendARP(gatewayIP, srcIP, srcMAC, handle); err != nil {
		return nil, err
	}
	sendTime := time.Now()
	// Wait 3 seconds for an ARP reply.
	for {
		if time.Since(sendTime) > time.Second*3 {
			break
		}
		data, _, err := handle.ReadPacketData()
		if err == pcap.NextErrorTimeoutExpired {
			continue
		} else if err != nil {
			return nil, err
		}
		packet := gopacket.NewPacket(data, layers.LayerTypeEthernet, gopacket.NoCopy)
		if arpLayer := packet.Layer(layers.LayerTypeARP); arpLayer != nil {
			arp := arpLayer.(*layers.ARP)
			if net.IP(arp.SourceProtAddress).Equal(net.IP(gatewayIP)) {
				return net.HardwareAddr(arp.SourceHwAddress), nil
			}
		}
	}
}
return nil, errors.New("timeout getting ARP reply")
}

func sendARP(gatewayIP net.IP, srcIP net.IP, srcMAC net.HardwareAddr, handle *pcap.Handle) error {
	eth := layers.Ethernet{
		SrcMAC:       srcMAC,
		DstMAC:       net.HardwareAddr{0xff, 0xff, 0xff, 0xff, 0xff, 0xff},
		EthernetType: layers.EthernetTypeARP,
	}
	arp := layers.ARP{
		AddrType:          layers.LinkTypeEthernet,
		Protocol:          layers.EthernetTypeIPv4,
		HwAddressSize:     6,
		ProtAddressSize:   4,
		Operation:         layers.ARPRequest,
		SourceHwAddress:   []byte(srcMAC),
		SourceProtAddress: []byte(srcIP),
		DstHwAddress:      []byte{0, 0, 0, 0, 0, 0},
		DstProtAddress:    []byte(gatewayIP),
	}
	buf := gopacket.NewSerializeBuffer()
	opts :=  gopacket.SerializeOptions{
		FixLengths:       true,
		ComputeChecksums: true,
	}
	err := gopacket.SerializeLayers(buf, opts, &eth, &arp)
	if err != nil {
		return err
	}
	
	return handle.WritePacketData(buf.Bytes())
}
