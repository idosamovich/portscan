package scanners

import (
	"net"
	"time"
	"crypto/rand"

	"github.com/google/gopacket"
	"github.com/google/gopacket/pcap"
	"github.com/google/gopacket/layers"
)

const (
	udpPayloadLen = 16
)

type udpPortScanner struct {
	*baseScanner
}


func NewUDPPortScanner(scanIP net.IP) (*udpPortScanner, error) {
	baseScanner, err := newBaseScanner(scanIP)
	return &udpPortScanner{baseScanner}, err
}


func (ups *udpPortScanner) Scan(srcPort uint16, dstPort uint16) (ScanResult, error) {
	handle, err := pcap.OpenLive(ups.iface.Name, maxPacketRead, false, pcap.BlockForever)
	if err != nil {
		return ScanResult{}, err
	}
	defer handle.Close()

	for i:=0; i < ups.packetsToSend; i++ {
                if err := ups.sendDgram(srcPort, dstPort, handle); err != nil {
                        return ScanResult{}, err
                }
        }

	return ups.getResult(srcPort, dstPort, handle)
}

func (ups *udpPortScanner) sendDgram(srcPort uint16, dstPort uint16, handle *pcap.Handle) error {
        eth := layers.Ethernet{
                SrcMAC:         ups.srcMAC,
                DstMAC:         ups.gatewayMAC,
                EthernetType:   layers.EthernetTypeIPv4,
        }
        ip := layers.IPv4{
                SrcIP:    ups.srcIP,
                DstIP:    ups.dstIP,
                Version:  4,
                TTL:      64,
                Protocol: layers.IPProtocolUDP,
        }
        udp := layers.UDP{
                SrcPort: layers.UDPPort(srcPort),
                DstPort: layers.UDPPort(dstPort),
                Length:  udpPayloadLen,
        }
        udp.SetNetworkLayerForChecksum(&ip)

	payload := make([]byte, udpPayloadLen)
	if _, err := rand.Read(payload); err != nil {
		return err
	}

        err := gopacket.SerializeLayers(ups.serializeBuffer, ups.serializeOptions, &eth, &ip, &udp, gopacket.Payload(payload))
        if err != nil {
                return err
        }

        return handle.WritePacketData(ups.serializeBuffer.Bytes())
}

func (ups *udpPortScanner) getResult(srcPort uint16, dstPort uint16, handle *pcap.Handle) (ScanResult, error) {
        // To calculate timeout for filtered ports.
        sendTime := time.Now()
        for {
                if time.Since(sendTime) > defaultFilteredTimeout {
                        return ScanResult{
                                Port: dstPort,
                                State: PortStateUnknown,
                        }, nil
                }

                nextPacketData, _, err := handle.ReadPacketData()
                if err != nil {
                        return ScanResult{}, err
                }

                packet := gopacket.NewPacket(nextPacketData, layers.LayerTypeEthernet, gopacket.NoCopy)

                if !ups.isPacketRelevant(packet, srcPort, dstPort) {
                        continue
                }

                if _, ok := packet.Layer(layers.LayerTypeUDP).(*layers.UDP); ok {
                        return ScanResult{
                                Port: dstPort,
                                State: PortStateOpen,
                        }, nil

		}

		if icmp, ok := packet.Layer(layers.LayerTypeICMPv4).(*layers.ICMPv4); ok &&
		   uint8(icmp.TypeCode.Type() >> 8) == layers.ICMPv4TypeDestinationUnreachable &&
		   uint8(icmp.TypeCode.Code()) == layers.ICMPv4CodePort {
                        return ScanResult{
                                Port: dstPort,
                                State: PortStateClosed,
                        }, nil

		}
        }
}

func (ups *udpPortScanner) isPacketRelevant(packet gopacket.Packet, srcPort uint16, dstPort uint16) bool {
        if packet.NetworkLayer() == nil {
                return false
        }
	
	ip, ok := packet.NetworkLayer().(*layers.IPv4)
        if !ok  {
                return false
        }

	if !ip.DstIP.Equal(ups.srcIP) && !ip.SrcIP.Equal(ups.dstIP) {
		return false
	}

	if ip.NextLayerType() == layers.LayerTypeUDP {
		// UDP Checks
        	if packet.TransportLayer() == nil {
                	return false
	        }

        	udp, ok := packet.TransportLayer().(*layers.UDP)
	        if !ok {
        	        return false
	        }

        	if udp.SrcPort != layers.UDPPort(dstPort) && udp.DstPort != layers.UDPPort(srcPort) {
                	return false
	        }

		return true
	}

	if ip.NextLayerType() != layers.LayerTypeICMPv4 {
		return false
	}

        return true
}

