package scanners

import (
	"net"
	"time"

	"github.com/google/gopacket"
	"github.com/google/gopacket/layers"
	"github.com/google/gopacket/pcap"
)


type tcpPortScanner struct {
	*baseScanner
}


func NewTCPPortScanner(scanIP net.IP) (*tcpPortScanner, error) {
	baseScanner, err := newBaseScanner(scanIP)
	return &tcpPortScanner{baseScanner}, err
}

func (tps *tcpPortScanner) Scan(srcPort uint16, dstPort uint16) (ScanResult, error) {
	handle, err := pcap.OpenLive(tps.iface.Name, maxPacketRead, true, pcap.BlockForever)
	if err != nil {
		return ScanResult{}, err
	}
	defer handle.Close()

	for i:=0; i < tps.packetsToSend; i++ {
		if err := tps.sendSyn(srcPort, dstPort, handle); err != nil {
			return ScanResult{}, err
		}
	}

	return tps.getResult(srcPort, dstPort, handle)
}

func (tps *tcpPortScanner) sendSyn(srcPort uint16, dstPort uint16, handle *pcap.Handle) error {
	eth := layers.Ethernet{
		SrcMAC:       	tps.srcMAC,
		DstMAC:		tps.gatewayMAC,
		EthernetType: 	layers.EthernetTypeIPv4,
	}
	ip := layers.IPv4{
		SrcIP:    tps.srcIP,
		DstIP:    tps.dstIP,
		Version:  4,
		TTL:      64,
		Protocol: layers.IPProtocolTCP,
	}
	tcp := layers.TCP{
		SrcPort: layers.TCPPort(srcPort),
		DstPort: layers.TCPPort(dstPort),
		SYN:     true,
	}
	tcp.SetNetworkLayerForChecksum(&ip)

	err := gopacket.SerializeLayers(tps.serializeBuffer, tps.serializeOptions, &eth, &ip, &tcp)
	if err != nil {
		return err
	}

	return handle.WritePacketData(tps.serializeBuffer.Bytes())
}

func (tps *tcpPortScanner) getResult(srcPort uint16, dstPort uint16, handle *pcap.Handle) (ScanResult, error) {
	// To calculate timeout for filtered ports.
	sendTime := time.Now()
	for {
		if time.Since(sendTime) > defaultFilteredTimeout {
			return ScanResult{
				Port: dstPort,
				State: PortStateFiltered,
			}, nil
		}

		nextPacketData, _, err := handle.ReadPacketData()
		if err != nil {
			return ScanResult{}, err
		}

		packet := gopacket.NewPacket(nextPacketData, layers.LayerTypeEthernet, gopacket.NoCopy)

		if !tps.isPacketRelevant(packet, srcPort, dstPort) {
			continue
		}

		tcp := packet.Layer(layers.LayerTypeTCP).(*layers.TCP)
		if tcp.RST {
			return ScanResult{
				Port: dstPort,
				State: PortStateClosed,
			}, nil
		}

		if tcp.SYN && tcp.ACK {
			return ScanResult{
				Port: dstPort,
				State: PortStateOpen,
			}, nil
		}
	}
}


func (tps *tcpPortScanner) isPacketRelevant(packet gopacket.Packet, srcPort uint16, dstPort uint16) bool {
	if packet.NetworkLayer() == nil {
		return false
	}

	if ip, ok := packet.NetworkLayer().(*layers.IPv4); !ok || (!ip.DstIP.Equal(tps.srcIP) && !ip.SrcIP.Equal(tps.dstIP)) {
		return false
	}

	if packet.TransportLayer() == nil {
		return false
	}

	tcp, ok := packet.TransportLayer().(*layers.TCP)
	if !ok {
		return false
	}

	if tcp.SrcPort != layers.TCPPort(dstPort) && tcp.DstPort != layers.TCPPort(srcPort) {
		return false
	}

	// If not RST or SYNACK
	if !tcp.RST && !(tcp.SYN && tcp.ACK) {
		return false
	}

	return true
}
