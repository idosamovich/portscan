package system

import (
	"fmt"
	"net"
	"time"

	"github.com/gammazero/workerpool"

	"ido/portscan/scan/scanners"
	"ido/portscan/util"
)

const (
	startScanMessage = "Starting to scan %v...\n\n"
	scanTCPMessage = "Scanning %d TCP ports"
	tcpResultsMessage = "\nResults for tcp scan:\n"

	scanUDPMessage = "Scanning %d UDP ports"
	udpResultsMessage = "\nResults for udp scan:\n"
)

type scanSystem struct {
	scanIP net.IP
	tcpScanner scanners.PortScanner
	udpScanner scanners.PortScanner

	numberOfWorkers int
}


func NewScanSystem(scanIP net.IP, numberOfWorkers int) (*scanSystem, error) {
	tcpScanner, err := scanners.NewTCPPortScanner(scanIP)
	if err != nil {
		return nil, err
	}

	udpScanner, err := scanners.NewUDPPortScanner(scanIP)
	if err != nil {
		return nil, err
	}

	return &scanSystem{
		tcpScanner: tcpScanner,
		udpScanner: udpScanner,
		scanIP: scanIP,
		numberOfWorkers: numberOfWorkers,
	}, nil
}

func (s *scanSystem) Scan(tcpPorts []uint16, udpPorts []uint16) error {
	fmt.Printf(startScanMessage, s.scanIP)
	var err error

	if len(tcpPorts) > 0 {
		err = s.scanTCP(tcpPorts)
		if err != nil {
			return err
		}
	}
	
	if len(udpPorts) > 0 {
		err = s.scanUDP(udpPorts)
		if err != nil {
			return err
		}
	}

	return nil
}

func (s *scanSystem) scanTCP(ports []uint16) (error) {
	fmt.Printf(scanTCPMessage, len(ports))
	results, err := s.scan(ports, s.tcpScanner)
	if err != nil {
		return err
	}

	fmt.Printf(tcpResultsMessage)
	s.printResults(results)
	return nil
}

func (s *scanSystem) scanUDP(ports []uint16) (error) {
	fmt.Printf(scanUDPMessage, len(ports))
	results, err := s.scan(ports, s.udpScanner)
	if err != nil {
		return err
	}

	fmt.Printf(udpResultsMessage)
	s.printResults(results)
	return nil
}

func (s *scanSystem) printResults(results map[uint16]scanners.PortState) {
	for port, state := range results {
		fmt.Printf("\tPort: %d is %s\n", port, scanners.PortStateTranslation[state])
	}
	fmt.Println()
}

func (s *scanSystem) scan(ports []uint16, scanner scanners.PortScanner) (map[uint16]scanners.PortState, error) {
	workerPool := workerpool.New(s.numberOfWorkers)
	answerChan := make(chan scanners.ScanResult, s.numberOfWorkers)
	errChan := make(chan error, s.numberOfWorkers)
	runningWorkers := 0

	for _, dstPort := range ports {
		srcPort := util.GetSrcPortNotUsed()
		dstPort := dstPort
		workerPool.Submit(func(){scanPort(scanner, srcPort, dstPort, answerChan, errChan)})
		runningWorkers++
	}

	results := map[uint16]scanners.PortState{}
	var err error
	for !workerPool.Stopped() {
		select {
		case result := <-answerChan:
			results[result.Port] = result.State
			runningWorkers--

		case err = <-errChan:
			fmt.Println()
			workerPool.Stop()
			break

		default:
			if runningWorkers == 0 {
				workerPool.StopWait()
			}
			fmt.Print(".")
			time.Sleep(time.Second)
		}
	}

	return results, err
}


func scanPort(scanner scanners.PortScanner, srcPort uint16, dstPort uint16, answerChan chan scanners.ScanResult, errChan chan error) {
	if result, err := scanner.Scan(srcPort, dstPort); err != nil {
		errChan<-err
	} else {
		answerChan<-result
	}
}
