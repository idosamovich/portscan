package main

import (
	"runtime"
	"net"
	"time"
	"flag"
	"math/rand"
	"fmt"
	"os"

	"ido/portscan/scan/system"
	"ido/portscan/util"
)



var (
	scanIP = ""
	tcpPorts = ""
	udpPorts = ""
	scanInterval = time.Duration(0)
	numOfScans = 0
)

func init() {
	flag.StringVar(&scanIP, "ip", "8.8.8.8", "The IP to scan.")
	flag.StringVar(&tcpPorts, "tcp", "", "TCP ports to scan. Can separate ports with , or set a range with -")
	flag.StringVar(&udpPorts, "udp", "", "UDP ports to scan.  Can separate ports with , or set a range with -")
	flag.DurationVar(&scanInterval, "i", 0, "The amount of time between scans. Use time durations such as h,m,s...")
	flag.IntVar(&numOfScans, "n", 1, "Number of scans to perform. If set to 0, will perform indefinately.")
}

func handleError(err error) {
	fmt.Fprintf(os.Stderr, "Error: %s\n", err)
	os.Exit(1)
}



func main() {
	rand.Seed(time.Now().UnixNano())
	flag.Parse()

	var tcpScanPorts, udpScanPorts []uint16
	var err error
	if tcpPorts != "" {
		tcpScanPorts, err = util.ParsePortRange(tcpPorts)
		if err != nil {
			handleError(err)
		}
	}

	if udpPorts != "" {
		udpScanPorts, err = util.ParsePortRange(udpPorts)
		if err != nil {
			handleError(err)
		}
	}

	scanSystem, err := system.NewScanSystem(net.ParseIP(scanIP), runtime.NumCPU())
	if err != nil {
		handleError(err)
	}

	for i:=0; i<numOfScans || numOfScans == 0; i++ {
		err = scanSystem.Scan(tcpScanPorts, udpScanPorts)
		if err != nil {
			handleError(err)
		}

		time.Sleep(scanInterval)
	}
}
