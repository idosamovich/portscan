package util


func UniqueSlice(s []uint16) []uint16 {
    keys := make(map[uint16]bool)
    list := []uint16{} 
    for _, entry := range s {
        if _, value := keys[entry]; !value {
            keys[entry] = true
            list = append(list, entry)
        }
    }    
    return list
}
