package util

import (
	"math/rand"
	"strings"
	"strconv"
	"fmt"
)

const (
	maxPort = 65535	// Max uint16

	// Arbitrarily selected so that we have a wide range of ports that also have a high chance of not being blocked.
	minSrcPort = 20000

	portRangesSeparator = ","
	portRangeIndicator = "-"
)


var usedSrcPorts = map[uint16]bool{}


func getRandomPort() uint16 {
	return uint16(rand.Intn(maxPort - minSrcPort) + minSrcPort)
}

func GetSrcPortNotUsed() uint16 {
	if len(usedSrcPorts) == (maxPort - minSrcPort) {
		// All srcPorts are used, create a new srcPort map:
		usedSrcPorts = map[uint16]bool{}
	}

	port := getRandomPort()
	for usedSrcPorts[port] {
		port = getRandomPort()
	}

	usedSrcPorts[port] = true

	return port
}

func ParsePortRange(portRange string) ([]uint16, error) {
	portRanges := strings.Split(portRange, portRangesSeparator)
	parsedPortRange := []uint16{}
	for _, portRange := range portRanges {
		if strings.Count(portRange, portRangeIndicator) > 1 {
			return nil, fmt.Errorf("PortRange format is invalid: %v", portRange)
		}

		if !strings.Contains(portRange, portRangeIndicator) {
			port, err := convertPortStrToUint16(portRange)
			if err != nil {
				return nil, err
			}
			parsedPortRange = append(parsedPortRange, port)
			continue
		}

		portRangeSeparated := strings.Split(portRange, portRangeIndicator)
		minPort, err := convertPortStrToUint16(portRangeSeparated[0])
		if err != nil {
			return nil, err
		}
		maxPort, err := convertPortStrToUint16(portRangeSeparated[1])
		if err != nil {
			return nil, err
		}

		if minPort > maxPort {
			return nil, fmt.Errorf("min port is larger than max port: %v-%v", minPort, maxPort)
		}

		for i:=minPort; i<maxPort+1; i++ {
			parsedPortRange = append(parsedPortRange, i)	
		}
	}

	return UniqueSlice(parsedPortRange), nil
}

func convertPortStrToUint16(portStr string) (uint16, error) {
	port, err := strconv.Atoi(portStr)
	if err != nil {
		return 0, err
	}
	if port > maxPort || port <= 0{
		return 0, fmt.Errorf("Port is too large or too small: %v", port)
	}

	return uint16(port), nil
}
